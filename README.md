# Scientific Friday Exercises

## Setup

1. Clone the repository `git clone https://gitlab.com/ChristianRavn/scifri-exercises.git`
2. (Optional) Create a virtual environment for the exercises
3. Install the requirements with `pip install -r requirements.txt`
4. Verify that everything works by running `pytest ex1`

## Additional

+ [Out of the Tar Pit][ofttp]: The talk is based heavily on this paper, which is a highly recommended read
+ [Pytest][pytest]: Documentation for the testing framework for future lookups
+ [Funcy][funcy]: Basic Python implementations of key functions for FP
+ [Toolz][toolz]: Very comprehensive library of FP functions. Is overkill in most cases
+ [Pyenv][pyenv]: Python version manager
+ [Pyenv-virtualenv][pyenv-venv]: Virtual environment plugin for Pyenv. I recommend this over Pythons own venv system

[ofttp]: http://curtclifton.net/papers/MoseleyMarks06a.pdf
[pytest]: https://docs.pytest.org/en/latest/
[funcy]: https://github.com/Suor/funcy
[toolz]: https://github.com/pytoolz/toolz/
[pyenv]: https://github.com/pyenv/pyenv
[pyenv-venv]: https://github.com/pyenv/pyenv-virtualenv
