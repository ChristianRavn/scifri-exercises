import filter_ex as f


def test_without_filter():
    l = [
        '# This is a comment',
        '# Also this',
        '1 2 3 4 5',
        '1 3 4 5',
        '1 2 3 5'
    ]
    exp_res = ['1 2 3 4 5', '1 3 4 5', '1 2 3 5']
    fn_res = f.without_filter(l)
    assert exp_res == fn_res

def test_with_filter():
    l = [
        '# This is a comment',
        '# Also this',
        '1 2 3 4 5',
        '1 3 4 5',
        '1 2 3 5'
    ]
    exp_res = ['1 2 3 4 5', '1 3 4 5', '1 2 3 5']
    fn_res = f.with_filter(l)
    assert exp_res == list(fn_res)
