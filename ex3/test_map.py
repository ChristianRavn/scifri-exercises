import map_ex as m


def test_without_map():
    l = ['1 2 3 4 5', '1 3 4 5', '1 2 3 5']
    exp_res = [
        ['1', '2', '3', '4', '5'],
        ['1', '3', '4', '5'],
        ['1', '2', '3', '5']
    ]
    fn_res = m.without_map(l)
    assert exp_res == fn_res

def test_with_map():
    l = ['1 2 3 4 5', '1 3 4 5', '1 2 3 5']
    exp_res = [
        ['1', '2', '3', '4', '5'],
        ['1', '3', '4', '5'],
        ['1', '2', '3', '5']
    ]
    fn_res = m.with_map(l)
    assert exp_res == list(fn_res)
