from functools import reduce


def without_reduce(sets):
    common = set(sets[0])
    for el in sets[1:]:
        common = set.intersection(el, common)
    return common

def with_reduce(ll):
    pass
