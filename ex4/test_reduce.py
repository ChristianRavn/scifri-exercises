import reduce_ex as r


def test_without_reduce():
    sets = [
        {'1', '2', '3', '4', '5'},
        {'1', '3', '4', '5'},
        {'1', '2', '3', '5'}
    ]
    exp_res = {'1', '3', '5'}
    fn_res = r.without_reduce(sets)
    assert exp_res == fn_res

def test_with_reduce():
    sets = [
        {'1', '2', '3', '4', '5'},
        {'1', '3', '4', '5'},
        {'1', '2', '3', '5'}
    ]
    exp_res = {'1', '3', '5'}
    fn_res = r.with_reduce(sets)
    assert exp_res == fn_res
